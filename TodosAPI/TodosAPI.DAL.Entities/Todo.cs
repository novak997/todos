﻿namespace TodosAPI.DAL.Entities
{
    public class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Date { get; set; }
    }
}