﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodosAPI.BLL.Exceptions
{
    public class BusinessLayerException : Exception
    {
        public BusinessLayerException() {}
        public BusinessLayerException(string message) : base(message) {}
    }
}
