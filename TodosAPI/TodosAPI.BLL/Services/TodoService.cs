﻿using TodosAPI.BLL.Contracts.Services;
using TodosAPI.BLL.Exceptions;
using TodosAPI.DAL.Contracts.Repositories;
using TodosAPI.DAL.Entities;
using TodosAPI.DAL.SQLClient.Exceptions;

namespace TodosAPI.BLL.Services
{
    public class TodoService : ITodoService
    {
        private readonly ITodoRepository _todoRepository;

        public TodoService(ITodoRepository todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public IEnumerable<Todo> GetTodosByDate(string date)
        {
            try
            {
                return _todoRepository.GetTodosByDate(CheckAndParseDate(date));
            }
            catch (DatabaseException ex)
            {
                throw ex;
            }
        }

        public Todo AddTodo(string title, string date)
        {
            try
            {
                if (title == null || title == "")
                {
                    throw new BusinessLayerException("Title cannot be empty");
                }
                return _todoRepository.AddTodo(title, CheckAndParseDate(date));
            }
            catch (DatabaseException ex)
            {
                throw ex;
            }
        }

        public int SetTodoDone(int id)
        {
            try
            {
                return _todoRepository.SetTodoDone(id);
            }
            catch (DatabaseException ex)
            {
                throw ex;
            }
        }

        private static DateTime CheckAndParseDate(string date)
        {
            bool success = DateTime.TryParseExact(date, "dd-MM-yyyy", 
                System.Globalization.CultureInfo.InvariantCulture, 
                System.Globalization.DateTimeStyles.None, out DateTime parsedDate);
            if (!success)
            {
                throw new BusinessLayerException("Invalid date format");
            }
            return parsedDate;
        }
    }
}
