﻿using TodosAPI.DAL.Entities;

namespace TodosAPI.DAL.Contracts.Repositories
{
    public interface ITodoRepository
    {
        public IEnumerable<Todo> GetTodosByDate(DateTime date);
        public Todo AddTodo(string title, DateTime date);
        public int SetTodoDone(int id);
    }
}
