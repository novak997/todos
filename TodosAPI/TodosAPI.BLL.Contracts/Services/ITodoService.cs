﻿using TodosAPI.DAL.Entities;

namespace TodosAPI.BLL.Contracts.Services
{
    public interface ITodoService
    {
        public IEnumerable<Todo> GetTodosByDate(string date);
        public Todo AddTodo(string title, string date);
        public int SetTodoDone(int id);
    }
}
