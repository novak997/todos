using Autofac;
using Autofac.Extensions.DependencyInjection;
using TodosAPI.BLL.Contracts.Services;
using TodosAPI.BLL.Services;
using TodosAPI.DAL.Contracts.Repositories;
using TodosAPI.DAL.SQLClient.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(container => {
    container.RegisterType<TodoRepository>().As<ITodoRepository>();
    container.RegisterType<TodoService>().As<ITodoService>();
});
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();

app.UseAuthorization();

app.MapControllers();

app.Run();
