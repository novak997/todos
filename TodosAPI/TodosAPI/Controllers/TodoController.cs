﻿using Microsoft.AspNetCore.Mvc;
using TodosAPI.BLL.Contracts.Services;
using TodosAPI.BLL.Exceptions;
using TodosAPI.Controllers.DTOs;
using TodosAPI.DAL.SQLClient.Exceptions;

namespace TodosAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
        private readonly ITodoService _todoService;

        public TodoController(ITodoService todoService)
        {
            _todoService = todoService;
        }

        [HttpPost]
        public IActionResult AddTodo([FromBody] TodoDTO todo)
        {
            try
            {
                return Ok(_todoService.AddTodo(todo.Title, todo.Date));
            }
            catch (DatabaseException ex)
            {
                return StatusCode(500, ex.Message);
            }
            catch (BusinessLayerException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetTodosByDate([FromQuery] string date)
        {
            try
            {
                return Ok(_todoService.GetTodosByDate(date));
            }
            catch (DatabaseException ex)
            {
                return StatusCode(500, ex.Message);
            }
            catch (BusinessLayerException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult SetTodoDone([FromQuery] int id)
        {
            try
            {
                return Ok(_todoService.SetTodoDone(id));
            }
            catch (DatabaseException ex)
            {
                return StatusCode(500, ex.Message);
            }
            catch (BusinessLayerException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
