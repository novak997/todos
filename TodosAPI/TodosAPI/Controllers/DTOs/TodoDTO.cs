﻿namespace TodosAPI.Controllers.DTOs
{
    public class TodoDTO
    {
        public string Title { get; set; }
        public string Date { get; set; }
    }
}
