﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using TodosAPI.DAL.Contracts.Repositories;
using TodosAPI.DAL.Entities;
using TodosAPI.DAL.SQLClient.Exceptions;

namespace TodosAPI.DAL.SQLClient.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        private readonly IConfiguration _configuration;

        public TodoRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<Todo> GetTodosByDate(DateTime date)
        {
            try
            {
                using SqlConnection connection = new(_configuration.GetConnectionString("Database"));
                SqlCommand command = new("dbo.uspGetTodosByDate", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                connection.Open();
                command.Parameters.AddWithValue("@date", date);
                SqlDataReader reader = command.ExecuteReader();
                List<Todo> todos = new();
                while (reader.Read())
                {
                    Todo todo = new()
                    {
                        Id = Convert.ToInt32(reader["TodoID"]),
                        Title = reader["title"].ToString(),
                        Date = reader["date"].ToString(),
                    };
                    todos.Add(todo);
                }
                return todos;
            }
            catch (SqlException)
            {
                throw new DatabaseException("A database related exception has occurred");
            }
        }

        public Todo AddTodo(string title, DateTime date)
        {
            try
            {
                using SqlConnection connection = new(_configuration.GetConnectionString("Database"));
                SqlCommand command = new("dbo.uspAddTodo", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                connection.Open();
                command.Parameters.AddWithValue("@title", title);
                command.Parameters.AddWithValue("@date", date);
                SqlDataReader reader = command.ExecuteReader();
                List<Todo> todos = new();
                while (reader.Read())
                {
                    Todo todo = new()
                    {
                        Id = Convert.ToInt32(reader["TodoID"]),
                        Title = reader["title"].ToString(),
                        Date = reader["date"].ToString(),
                    };
                    todos.Add(todo);
                }
                return todos[0];
            }
            catch (SqlException ex)
            {
                throw ex.Message == "Title taken" ? new DatabaseException("Title taken") : new DatabaseException("A database related exception has occurred");
            }
        }

        public int SetTodoDone(int id)
        {
            try
            {
                using SqlConnection connection = new(_configuration.GetConnectionString("Database"));
                SqlCommand command = new("dbo.uspSetTodoDone", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                connection.Open();
                command.Parameters.AddWithValue("@id", id);
                command.ExecuteNonQuery();
                return id;
            }
            catch (SqlException)
            {
                throw new DatabaseException("A database related exception has occurred");
            }
        }
    }
}
