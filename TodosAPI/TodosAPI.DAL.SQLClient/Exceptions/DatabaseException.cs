﻿namespace TodosAPI.DAL.SQLClient.Exceptions
{
    public class DatabaseException : Exception
    {
        public DatabaseException() {}
        public DatabaseException(string message) : base(message) {}
    }
}
