-The Todos folder contains the todos Flutter app, the TodosAPI folder contains the API

-The todos_script.sql file will create the necessary SQL Server table and stored procedures

-The database connection string is located in the appsettings.json file in the TodosAPI project as "Database"

-Prerequisites for running the Flutter app:
    -Flutter
    -Visual Studio Code with the Flutter plugin or Android Studio
    -A real Android device with USB debugging enabled or an emulated Android device

-The localhost address needs to be tunnelled to be accessed by the device, using tools such as:
    -ngrok
    -Conveyor by Keyoti, an addon for Visual Studio

-The API URL is located in todos\lib\data\constants\constants.dart as baseUrl, and it needs to be updated with the tunnelled address

-The date for which the TODOs are displayed is located in the same file as currentDate, and can be updated ("dd-MM-yyyy" format)

-To install the needed dependencies the following command needs to be run in the terminal first:
    flutter pub get

-Finally, run the app, with or without debugging, using the editor's commands, or in the terminal:
    -with debugging:
        flutter run
    -without debugging:
        flutter run --profile

-NOTE: When changing the code while the Flutter app is running, a restart instead of a hot reload is recommended to avoid any errors