USE [todos]
GO
/****** Object:  Table [dbo].[Todos]    Script Date: 10.5.2022. 22:26:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Todos](
	[TodoID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[Date] [date] NOT NULL,
	[IsDone] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TodoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Todos] ADD  DEFAULT ((0)) FOR [IsDone]
GO
/****** Object:  StoredProcedure [dbo].[uspAddTodo]    Script Date: 10.5.2022. 22:26:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspAddTodo]
   @title nvarchar(255),
   @date date
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @numberOfTodosWithSameTitle int
	SELECT @numberOfTodosWithSameTitle = COUNT(*) FROM Todos WHERE IsDone = 0 AND Date = @date AND UPPER(Title) = UPPER(@title)
	IF (@numberOfTodosWithSameTitle > 0)
	BEGIN
		RAISERROR('Title taken', 16, 1)
		RETURN
	END

	INSERT INTO Todos(Title, Date) VALUES (@title, @date)

	SELECT TOP 1 * FROM Todos ORDER BY TodoID DESC
END
GO
/****** Object:  StoredProcedure [dbo].[uspGetTodosByDate]    Script Date: 10.5.2022. 22:26:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetTodosByDate]
   @date date
AS
BEGIN
   SET NOCOUNT ON
	SELECT * FROM Todos WHERE Date = @date AND IsDone = 0
END
GO
/****** Object:  StoredProcedure [dbo].[uspSetTodoDone]    Script Date: 10.5.2022. 22:26:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspSetTodoDone]
   @id int
AS
BEGIN
   SET NOCOUNT ON
	UPDATE Todos
	SET IsDone=1
	WHERE TodoID=@id
END
GO
