import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todos/bloc/cubit/todo_cubit.dart';
import 'package:todos/data/repositories/todo_repository.dart';
import 'package:todos/presentation/pages/todo_screen.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  // Fixes a certificate problem during development
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  // Fixes a certificate problem during development
  HttpOverrides.global = MyHttpOverrides();
  runApp(const TodoApp());
}

class TodoApp extends StatelessWidget {
  const TodoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TODO App',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // Inject the repository
      home: RepositoryProvider<TodoRepository>(
        create: (context) => TodoRepository(),
        child: BlocProvider<TodoCubit>(
          create: (context) => TodoCubit(
            todoRepository: context.read<TodoRepository>(),
            context: context,
          ),
          child: const TodoScreen(),
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
