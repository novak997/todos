import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class Quote extends StatelessWidget {
  final String quote;
  final String author;

  const Quote({
    Key? key,
    required this.quote,
    required this.author,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 15.0,
        left: 40.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: Center(
              // Using AutoSizeText for automatic resizing of the quotes within their boundaries
              child: AutoSizeText.rich(
                TextSpan(
                  text: quote + "\n",
                  children: <TextSpan>[
                    TextSpan(
                      text: "- " + author,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                ),
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
