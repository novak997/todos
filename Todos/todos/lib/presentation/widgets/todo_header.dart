import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:todos/bloc/cubit/todo_cubit.dart';
import 'package:todos/data/constants/constants.dart';
import 'package:todos/presentation/dialogs/add_todo_dialog.dart';
import 'package:todos/presentation/widgets/quote.dart';

class TodoHeader extends StatefulWidget {
  const TodoHeader({Key? key}) : super(key: key);

  @override
  State<TodoHeader> createState() => _TodoHeaderState();
}

class _TodoHeaderState extends State<TodoHeader> {
  String _quote = "";
  String _author = "";

  @override
  Widget build(BuildContext context) {
    bool isDateValid = true;
    try {
      DateFormat("dd-MM-yyyy").parseStrict(currentDate);
    } catch (e) {
      isDateValid = false;
    }
    return AppBar(
      centerTitle: false,
      // add button
      title: IconButton(
        splashColor: Colors.white,
        onPressed: () {
          addTodoDialog(context: context);
        },
        icon: const Icon(
          Icons.add_circle_outline,
          color: Colors.white,
        ),
      ),
      flexibleSpace: Container(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 30.0,
            bottom: 10.0,
          ),
          child: Row(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      const SizedBox(
                        width: 15.0,
                      ),
                      // calendar icon
                      const Icon(
                        Icons.calendar_month,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        width: 5.0,
                      ),
                      // current date
                      Text(
                        isDateValid
                            ? currentDate.replaceAll("-", "/")
                            : "Invalid date format",
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Expanded(
                // BlocBuilder which builds the Quote widget
                child: BlocBuilder<TodoCubit, TodoState>(
                  builder: (context, state) {
                    if (state is TodosRetrieved) {
                      _quote = state.quote;
                      _author = state.author;
                    } else if (state is GetTodosError) {
                      _quote = state.quote;
                      _author = state.author;
                    }
                    return Quote(
                      quote: _quote,
                      author: _author,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        // background image
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/header-bg.jpg'),
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    );
  }
}
