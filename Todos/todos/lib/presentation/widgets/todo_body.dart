import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todos/bloc/cubit/todo_cubit.dart';
import 'package:todos/data/constants/constants.dart';
import 'package:todos/data/models/todo.dart';
import 'package:todos/presentation/widgets/snack_bar.dart';
import 'package:todos/presentation/widgets/todo_item.dart';

class TodoBody extends StatefulWidget {
  const TodoBody({Key? key}) : super(key: key);

  @override
  State<TodoBody> createState() => _TodoBodyState();
}

class _TodoBodyState extends State<TodoBody> {
  List<Todo> _todos = [];

  @override
  Widget build(BuildContext context) {
    // BlocBuilder which builds the list of todos
    return BlocBuilder<TodoCubit, TodoState>(
      builder: (context, state) {
        if (state is TodoLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is TodosRetrieved) {
          _todos = state.todos;
        } else if (state is TodoAdded) {
          _todos.add(state.todo);
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            snackBar(message: "TODO successfully created!", context: context);
          });
        } else if (state is GetTodosError) {
          WidgetsBinding.instance?.addPostFrameCallback(
              (_) => snackBar(message: state.message, context: context));
        } else if (state is AddOrSetTodoDoneError) {
          WidgetsBinding.instance?.addPostFrameCallback(
              (_) => snackBar(message: state.message, context: context));
        } else if (state is TodoDone) {
          _todos.removeWhere((todo) => todo.id == state.id);
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            snackBar(message: "TODO successfully completed!", context: context);
          });
        }
        return RefreshIndicator(
          // pull down to refresh
          onRefresh: () async => await Future.delayed(
            const Duration(milliseconds: 200),
            () {
              BlocProvider.of<TodoCubit>(context).getTodosByDate();
            },
          ),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight:
                      MediaQuery.of(context).size.height - headerHeight - 35.0),
              child: _todos.isNotEmpty
                  ? Column(
                      children: List<Widget>.from(
                        _todos.map(
                          (todo) => TodoItem(
                            key: ValueKey(
                              todo.id,
                            ),
                            todo: todo,
                          ),
                        ),
                      ),
                    )
                  : const Center(
                      child: Text(
                        "No TODOs for the day!",
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    ),
            ),
          ),
        );
      },
    );
  }
}
