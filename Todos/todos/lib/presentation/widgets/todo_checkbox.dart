import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todos/bloc/cubit/todo_cubit.dart';

class TodoCheckbox extends StatefulWidget {
  final int id;

  const TodoCheckbox({Key? key, required this.id}) : super(key: key);

  @override
  State<TodoCheckbox> createState() => _TodoCheckboxState();
}

class _TodoCheckboxState extends State<TodoCheckbox> {
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Checkbox(
      value: _isChecked,
      onChanged: (bool? value) async {
        if (!_isChecked) {
          // Changing the checkbox to checked
          setState(() {
            _isChecked = value!;
          });
          await Future.delayed(
            const Duration(seconds: 1),
            () {
              BlocProvider.of<TodoCubit>(context).setTodoDone(id: widget.id);
            },
          );
        }
      },
    );
  }
}
