import 'package:flutter/material.dart';
import 'package:todos/data/models/todo.dart';
import 'package:todos/presentation/widgets/todo_checkbox.dart';

class TodoItem extends StatelessWidget {
  final Todo todo;

  const TodoItem({Key? key, required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(todo.title),
            ),
            TodoCheckbox(id: todo.id)
          ],
        ),
      ),
    );
  }
}
