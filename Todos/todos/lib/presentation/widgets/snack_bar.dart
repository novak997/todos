// Used for displaying feedback when operations are performed
// Messages of success or details of an error

import 'package:flutter/material.dart';

snackBar({required String message, required context}) {
  final snackBar = SnackBar(
    content: Text(message),
    action: SnackBarAction(
      label: "OK",
      onPressed: () {},
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
