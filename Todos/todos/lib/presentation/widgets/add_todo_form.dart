import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todos/bloc/cubit/todo_cubit.dart';

class AddTodoForm extends StatelessWidget {
  final BuildContext context;

  const AddTodoForm({Key? key, required this.context}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    var titleController = TextEditingController();
    return Form(
      key: formKey,
      child: Column(
        children: <Widget>[
          Card(
            child: TextFormField(
              controller: titleController,
              textInputAction: TextInputAction.next,
              autofocus: true,
              decoration: const InputDecoration(
                labelText: "Title",
                contentPadding: EdgeInsets.only(left: 15.0),
              ),
              validator: (value) {
                // check if the field is empty
                if (value == null || value.trim() == "") {
                  return "Title cannot be empty";
                }
                return null;
              },
            ),
          ),
          const SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("BACK"),
              ),
              ElevatedButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    Navigator.pop(context);
                    BlocProvider.of<TodoCubit>(this.context)
                        .addTodo(title: titleController.text.trim());
                  }
                },
                child: const Text("CONFIRM"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
