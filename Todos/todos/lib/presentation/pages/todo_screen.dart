import 'package:flutter/material.dart';
import 'package:todos/data/constants/constants.dart';
import 'package:todos/presentation/widgets/todo_body.dart';
import 'package:todos/presentation/widgets/todo_header.dart';

class TodoScreen extends StatelessWidget {
  const TodoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(headerHeight),
        child: TodoHeader(),
      ),
      body: TodoBody(),
    );
  }
}
