// The dialog which appears when the Add button is tapped
// Contains the form for adding a new todo

import 'package:flutter/material.dart';
import 'package:todos/presentation/widgets/add_todo_form.dart';

Future addTodoDialog({required context}) {
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (_) => WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: const Text("Create a TODO item"),
        insetPadding: const EdgeInsets.only(
          left: 5.0,
          right: 5.0,
        ),
        content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: AddTodoForm(
              context: context,
            ),
          ),
        ),
      ),
    ),
  );
}
