// The cubits are responsible for state management of the app
// They are the business logic part in the Bloc pattern
// a subtype of blocs
// This one is used by the TodoScreen
// BlocProviders call these methods within the UI
// The cubit then emits states which trigger the BlocBuilders listening to them

import 'dart:convert';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:todos/data/constants/constants.dart';
import 'package:todos/data/models/todo.dart';
import 'package:todos/data/repositories/todo_repository.dart';

part 'todo_state.dart';

class TodoCubit extends Cubit<TodoState> {
  final TodoRepository todoRepository;
  final BuildContext context;

  TodoCubit({required this.todoRepository, required this.context})
      : super(TodoInitial()) {
    // Get the TODOs on initialization of the cubit
    getTodosByDate();
  }

  void getTodosByDate() async {
    emit(TodoLoading());
    // Get a random quote from a predefined JSON file
    String quotes = await DefaultAssetBundle.of(context)
        .loadString("assets/data/quotes.json");
    List quotesJson = json.decode(quotes);
    Random random = Random();
    int randomIndex = random.nextInt(quotesJson.length);
    String quote = quotesJson[randomIndex]["text"];
    String author = quotesJson[randomIndex]["from"];
    final response = await todoRepository.getTodosByDate();
    response.fold(
      (todos) => emit(
        TodosRetrieved(
          todos: todos,
          quote: quote,
          author: author,
        ),
      ),
      (failure) => emit(
        GetTodosError(
          message: failure.message,
          quote: quote,
          author: author,
        ),
      ),
    );
  }

  void addTodo({required String title}) async {
    final response = await todoRepository.addTodo(todoDto: {
      "title": title,
      "date": currentDate,
    });
    response.fold(
      (todo) => emit(
        TodoAdded(todo: todo),
      ),
      (failure) => emit(
        AddOrSetTodoDoneError(message: failure.message),
      ),
    );
  }

  void setTodoDone({required int id}) async {
    final response = await todoRepository.setTodoDone(id: id);
    response.fold(
      (id) => emit(
        TodoDone(id: id),
      ),
      (failure) => emit(
        AddOrSetTodoDoneError(message: failure.message),
      ),
    );
  }
}
