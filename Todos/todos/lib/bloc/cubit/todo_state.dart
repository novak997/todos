// All the predefined states to be used by the cubit

part of 'todo_cubit.dart';

@immutable
abstract class TodoState {}

class TodoInitial extends TodoState {}

class TodoLoading extends TodoState {}

class TodosRetrieved extends TodoState {
  final List<Todo> todos;
  final String quote;
  final String author;

  TodosRetrieved({
    required this.todos,
    required this.quote,
    required this.author,
  });
}

class TodoAdded extends TodoState {
  final Todo todo;

  TodoAdded({required this.todo});
}

class TodoDone extends TodoState {
  final int id;

  TodoDone({required this.id});
}

class GetTodosError extends TodoState {
  final String message;
  final String quote;
  final String author;

  GetTodosError({
    required this.message,
    required this.quote,
    required this.author,
  });
}

class AddOrSetTodoDoneError extends TodoState {
  final String message;

  AddOrSetTodoDoneError({required this.message});
}
