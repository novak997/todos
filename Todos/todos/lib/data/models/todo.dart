class Todo {
  final int id;
  final String title;
  final String date;

  Todo(this.id, this.title, this.date);

  Todo.fromJson(Map json)
      : id = json["id"],
        title = json["title"],
        date = json["date"];
}
