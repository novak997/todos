// The repository is responsible for sending and receiving data from the API

import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart';
import 'package:todos/data/constants/constants.dart';
import 'package:todos/data/errors/failure.dart';
import 'package:todos/data/models/todo.dart';

class TodoRepository {
  Future<Either<List<Todo>, Failure>> getTodosByDate() async {
    try {
      final response = await get(
        Uri.parse("$baseUrl/api/todo?date=$currentDate"),
        headers: header,
      );
      if (response.statusCode == 200) {
        return Left(
          List<Todo>.from(
            json.decode(response.body).map((todo) => Todo.fromJson(todo)),
          ),
        );
      }
      return Right(Failure(message: response.body));
    } catch (e) {
      return Right(Failure(message: "Connection issues"));
    }
  }

  Future<Either<Todo, Failure>> addTodo({
    required Map<String, String> todoDto,
  }) async {
    try {
      final response = await post(
        Uri.parse("$baseUrl/api/todo"),
        body: json.encode(todoDto),
        headers: header,
      );
      if (response.statusCode == 200) {
        return Left(Todo.fromJson(json.decode(response.body)));
      }
      return Right(Failure(message: response.body));
    } catch (e) {
      return Right(Failure(message: "Connection issues"));
    }
  }

  // The reason why HTTP DELETE method is used since this is basically logical deletion of the todo
  Future<Either<int, Failure>> setTodoDone({required int id}) async {
    try {
      final response = await delete(
        Uri.parse("$baseUrl/api/todo?id=$id"),
        headers: header,
      );
      if (response.statusCode == 200) {
        return Left(id);
      }
      return Right(Failure(message: response.body));
    } catch (e) {
      return Right(Failure(message: "Connection issues"));
    }
  }
}
